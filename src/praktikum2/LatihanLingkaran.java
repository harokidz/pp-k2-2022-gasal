package praktikum2;

import java.util.Scanner;


public class LatihanLingkaran {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //diketahui
        float pi = 22f / 7f;
        
        System.out.print("Masukkan nilai jari-jari = ");
        float jariJari = scan.nextFloat();

        //ditanya keliling dan luas dari lingkaran
        //rumus : 
        //k = pi * r * 2;
        //l = pi * r * r;
        float kelilingLingkaran = pi * jariJari * 2;
        float luasLingkaran = pi * jariJari * jariJari;

        //hasil 
        System.out.println("Keliling Lingkaran = " + kelilingLingkaran);
        System.out.println("Luas Lingkaran = " + luasLingkaran);
    }
}
