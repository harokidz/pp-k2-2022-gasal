package praktikum2;

import java.util.Scanner;

public class TugasBola {

    public static void main(String[] args) {
         Scanner scan = new Scanner(System.in);
        //diketahui
        float pi = 22f / 7f;
        
        System.out.print("Masukkan nilai jari-jari = ");
        float r = scan.nextFloat();

        //ditanya keliling dan luas dari lingkaran
        //rumus : 
        //v = 4/3 * pi * r * *r * r;
        //l = 4 * pi * r * r;
        float v = (4f / 3f) * pi * r * r * r;
        float l = 4 * pi * r * r;
        //hasil 
//        System.out.println("Jari jari Bola = " + r);
        System.out.println("Volume Bola = " + v);
        System.out.println("Luas Permukaan Bola = " + l);
    }

}
