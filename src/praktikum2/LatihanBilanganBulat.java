package praktikum2;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class LatihanBilanganBulat {

    public static void main(String[] args) {
        //1. Buat input dengan type data integer
        int bilangan1 = 15000;
        int bilangan2 = 20000;

        //2. Coba type data byte
        //karna type datanya beda, maka hasil jumlah harus di casting / ubah type data
        //menjadi type data byte.
        byte hasilByte = (byte) (bilangan1 + bilangan2);
        System.out.println("Hasil Penjumlahan dengan Byte = " + hasilByte);

        //3.Coba type data short
        short hasilShort = (short) (bilangan2 / bilangan1);
        System.out.println("Hasil Bagi dengan Short = " + hasilShort);
        
        //4.Coba type data integer
        int hasilInteger = bilangan1 - bilangan2;
        System.out.println("Hasil Pengurangan dengan Integer = " + hasilInteger);

        //5.Coba type data Long
        long hasilLong = bilangan1 * bilangan2;
        System.out.println("Hasil Perkalian dengan Long = " + hasilLong);

    }
}
