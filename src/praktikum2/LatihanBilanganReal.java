package praktikum2;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class LatihanBilanganReal {

    public static void main(String[] args) {
        int a = 10;
        int b = 3;
        float aFloat = 10f;
        float bFloat = 3f;
        double aDb = 10d;
        double bDb = 3d;
        float c = (float) a / (float) b;
        float d = aFloat / bFloat;
        double e = (double) a / (double) b;
        double f = aDb / bDb;
        System.out.println("Hasil Bagi = " + c);
        System.out.println("Hasil Bagi = " + d);
        System.out.println("Hasil Bagi = " + e);
        System.out.println("Hasil Bagi = " + f);
    }
}
